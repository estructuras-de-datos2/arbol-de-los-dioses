#if !defined(_GOD_)
#define _GOD_

/**
 * @file God.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>

using namespace std;

class God {
    private:
        /* Attributes */
        string name;
        int followersQuantity;
        int numberOfGoD;
        int id;
        bool isEmpty = true;
    public:
        /**
         * @brief Construct a new God object.
         * With parameters.
         * @param pName 
         * @param pValue 
         */
        God(string pName, int pValue){
            this->name = pName;
            this->followersQuantity = pValue;
            this->isEmpty = false;
        }
        /**
         * @brief Construct a new God object.
         * No parameters.
         */
        God() {
            this->name="";
            this->followersQuantity=0;
            this->numberOfGoD=0;
            this->id=0;
        }

        // getters & setters
        string getName() {
            return this->name;
        }
        void setName(string pName) {
            this->name = pName;
        }
        int getFollowersQuantity() {
            return this->followersQuantity;
        }
        void setFollowersQuantity(int pQuantity) {
            this->followersQuantity = pQuantity;
        }
        int getNumberOfGod() {
            return this->numberOfGoD;
        }
        void setNumberOfGod(int pNumberId) {
            this->numberOfGoD = pNumberId;
        }
        int getId() {
            return this->id;
        }
        void setId(int pIndex) {
            this->id = pIndex;
        }
        bool emptyGod(){
            return this->isEmpty;
        }
        string* getAdressName() {
            return &this->name;
        }
};

#endif // _GOD_
