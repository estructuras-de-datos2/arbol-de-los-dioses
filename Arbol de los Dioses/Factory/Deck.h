#if !defined(_DECK_)
#define _DECK_ 

/**
 * @file Deck.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <stack>
#include "Card.h"
using namespace std;

class Deck {
    private:
        /* Attributes */
        int currentDeckSize = 0;
        int graveSize=0;
        stack<Card> *deckOfCards = new stack<Card>(); // metodo shuffle para std::barajar con vectores
        stack<Card> *graveyard = new stack<Card>();
    public:
        /**
         * @brief Construct a new Deck object.
         * No parameters.
         */
        Deck() {
            ;
        }

        // getters & setters.
        void updateSizeDeck() {
            this->currentDeckSize = deckOfCards->size();
        }
        int getCurrentDeckSize() {
            return this->currentDeckSize;
        }
        void setCurrentDeckSize(int pQuantity) {
            this->currentDeckSize = pQuantity;
        }
        void plusCurrentDeckSize(int pQuantity) {
            this->currentDeckSize += pQuantity;
        }
        void minusCurrentDeckSize(int pQuantity) {
            this->currentDeckSize -= pQuantity;
        }
        void plusGraveyard(int pValue) {
            this->graveSize+=pValue;
        }
        void minusGraveyrad(int pValue) {
            this->graveSize-=pValue;
        }
        int getGraveSize() {
            return this->graveSize;
        }
        stack<Card> *&getDeck() {
            return this->deckOfCards;
        }
        void setDeck(stack<Card> *&pDeck) {
            this->deckOfCards = pDeck;
        }
        stack<Card> *&getGraveyard() {
            return this->graveyard;
        }
        void printDeck() {
            stack<Card> *cards = deckOfCards;
            int cont = 0;
            while (!cards->empty()) {
                cout <<"\n" << ++cont << " -> "<<cards->top().getType() << endl;
                cards->pop();
            }
        }
};


#endif // _DECK_
