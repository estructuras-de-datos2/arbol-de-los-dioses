#if !defined(_CRAFTING_)
#define _CRAFTING_

/**
 * @file Crafting.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <stack>
#include <queue>
#include <functional>
#include <vector>
#include "Deck.h"
#include "Card.h" 
#include "God.h"
#include "JsonData.h"
#include "hashTable.h"
#include "avlTree.h"

// Calls of prototypes.
Deck* craftDeck();
priority_queue<int> *craftGodsQueue();
void printQueue(priority_queue<int> *pQueue) ;
void pushStackToQueue(queue<God> *&pStack, priority_queue<int> *&pQueue);
void gettinIndex(stack<God> *g);
MapADT craftHasTable();
God obtenerDios(queue<God> *pStack, int pValue);

/**
 * @brief Create and return deck of cards.
 * 
 * @return Deck* 
 */
Deck* craftDeck() {
    stack<Card> *cardsObjects = new stack<Card>();
    updatePrimalCards(cardsObjects);
    stack<Card> *deckOfCards = new stack<Card>();
    Deck *newDeck = new Deck();

    while(cardsObjects->empty() == false) {
        for (int iter = 0; iter < cardsObjects->top().getQuantity(); iter++) {
            deckOfCards->push(cardsObjects->top());
        }
        cardsObjects->pop();
    }
    
    // Setting deck of cards
    newDeck->setDeck(deckOfCards);

    return newDeck;
}

/**
 * @brief Create and return the queue by priority according to followers.
 * 
 * @return priority_queue<int>* 
 */
priority_queue<int> *craftGodsQueue() {
    priority_queue<int> *godQueueTurn = new priority_queue<int>();
    //print_queue(godQueueTurn);
    queue<God> *godis = updateGodsStack();
    pushStackToQueue(godis, godQueueTurn);
    return godQueueTurn;
}

/**
 * @brief shows the queue by priority in console.
 * 
 * @param pQueue 
 */
void printQueue(priority_queue<int> *pQueue) {
    priority_queue<int> *tempNum = new priority_queue<int>();
    while (!pQueue->empty()) {
        cout << pQueue->top() << " - ";
        tempNum->push(pQueue->top());
        pQueue->pop();
    }
    while (!tempNum->empty()) {
        pQueue->push(tempNum->top());
        tempNum->pop();
    }
}

/**
 * @brief Load the information of the followers in the queue by priority.
 * 
 * @param pStack 
 * @param pQueue 
 */
void pushStackToQueue(queue<God> *&pStack, priority_queue<int> *&pQueue) {
    while (!pStack->empty()) {
        //cout<<pStack->top().getName()<<pStack->top().getFollowersQuantity()<<endl;
        pQueue->push(pStack->front().getFollowersQuantity());
        pStack->pop();
    }
}

/**
 * @brief Create and load the data into the hash table.
 * 
 * @return MapADT 
 */
MapADT craftHasTable() {
    queue<God> *gods = updateGodsStack();
    MapADT hashTable;
    while (!gods->empty()) {
        hashTable.insert(gods->front().getName(), to_string(gods->front().getFollowersQuantity()));
        gods->pop();
    }
    return hashTable;
}

/**
 * @brief Create and load the information in the avl tree.
 * 
 * @param pStack 
 * @return BiTree 
 */
BiTree treeCargar(queue<God> *pStack) {
    queue<God> *tempGods = new queue<God>();
    BiTree avlTree = NULL;
    while (!pStack->empty()){
        tempGods->push(pStack->front());
        insertAvlTree(avlTree,pStack->front().getFollowersQuantity(), pStack->front().getName());
        pStack->pop();
    }
    while (!tempGods->empty()) {
        /* code */
        pStack->push(tempGods->front());
        tempGods->pop();
    }
    return avlTree;
}

#endif // _CRAFTING_