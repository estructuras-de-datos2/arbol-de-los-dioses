#if !defined(_GAME_)
#define _GAME_

/**
 * @file Game.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <algorithm>    // random_shuffle
#include <vector>       // vector
#include <ctime>        // time
#include <cstdlib>      // rand, srand
#include <stdio.h>
#include <stdlib.h>
#include "sleep.h"
#include "Crafting.h"
#include "menu.h"
#include "God.h"

using namespace std;

// Prototypes of functions.
int asginPlayer(priority_queue<int> *pPlayers);
void aplicationEffect(Card pCard, int &pGodA, int &pGodB, BiTree &pTree, Deck *pDeck);
void shuffleDeck(stack<Card> *pDeck);
Card dropCard(Deck *pDeck);
void printData(Card pCard, string a, string b, Deck *pDeck, int pFollowers[]);

/**
 * @brief Main Void 
 */
void runGame() { 
    /* DATA */
    // DECK
    Deck *newDeck = new Deck();
    newDeck = craftDeck(); 
    stack<Card> *deck = new stack<Card>();
    deck = newDeck->getDeck(); 
    stack<Card> *graveyard = new stack<Card>();
    graveyard = newDeck->getGraveyard();
    newDeck->updateSizeDeck();
    // INFORMACION DE LOS 20 DIOSES
    queue<God> *gods = (queue<God>*)malloc(sizeof(queue<God>));
    gods = updateGodsStack();
    // QUEUE BY PRIORITY
    priority_queue<int> *players = new priority_queue<int>();
    players = craftGodsQueue();
    // HASH TABLE
    MapADT hashTable = craftHasTable();
    // AVL TREE.
    BiTree avlTree = treeCargar(gods);
    BiTree playerOne = NULL, playerTwo = NULL;
    // CARD
    Card card;
    // PLAYERS
    string nameA, nameB;
    int quantityFollowers[4];
    // MENU VARIABLES 
    bool exit = false;
    int choice;
    
    while (exit != true) {
        shuffleDeck(newDeck->getDeck()); // I shuffle the deck
        //printQueue(players);;
        menu();
        cin >> choice;
        switch(choice){
            case 1:
                // Obtaining Gods.
                quantityFollowers[0] = asginPlayer(players); // I get first from the queue.
                quantityFollowers[1] = quantityFollowers[0]; // Save the old values of followers
                playerOne = searchAvlTree(avlTree, quantityFollowers[0]);
                try{
                    nameA = playerOne->god->getName();
                }   catch (const exception& er) {
                    cout<< "Got Segmentation Fault."<<endl;
                }
                
                quantityFollowers[2] = asginPlayer(players); // I get second from the queue.
                quantityFollowers[3] = quantityFollowers[2]; // Save the old values of followers
                playerTwo = searchAvlTree(avlTree, quantityFollowers[2]);
                try{
                    nameB = playerTwo->god->getName();
                } catch(const exception& er) {
                    cout<< "Got Segmentation Fault."<<endl;
                }

                // I get the card.
                card = dropCard(newDeck);

                // Nodes to remove. Old ones.
                deleteAvlNode(avlTree, quantityFollowers[0]);
                deleteAvlNode(avlTree, quantityFollowers[2]);

                // I apply effect of the card.
                aplicationEffect(card, quantityFollowers[0], quantityFollowers[2], avlTree, newDeck);

                // I print Information.
                printData(card, nameA, nameB, newDeck, quantityFollowers);

                // I put the Gods back into the avl tree.
                if (quantityFollowers[0] != quantityFollowers[2]) {
                    insertAvlTree(avlTree, quantityFollowers[0], nameA);
                    insertAvlTree(avlTree, quantityFollowers[2], nameB);
                }
                else {
                    insertAvlTree(avlTree, quantityFollowers[0], nameA);
                    insertAvlTree(avlTree, quantityFollowers[2]-1, nameB);
                }
                // I update my hashTable.
                hashTable.insert(nameA, to_string(quantityFollowers[0]));
                hashTable.insert(nameB, to_string(quantityFollowers[2]));

                // Re-entry to the queue by priority the followers.
                if (card.getType().compare("NewGod") == 0) {
                    players->push(quantityFollowers[0]+quantityFollowers[2]);
                } 
                players->push(quantityFollowers[0]);
                players->push(quantityFollowers[2]);
                break;
            case 2:
                hashTable.print(); // I print the information from the hash table.
                sleepGame(1);
                break;
            case 3:
                cout<<"____________POSTORDEN__________"<<endl;
                postOrden(avlTree);
                exit = true;
                sleepGame(1);
                cout << farewell();
                break;      
            default:
                cout<<"Ingrese un valor asociado."<<endl;
                break;
        }
    }
}

/**
 * @brief Displays the information of the current play.
 * 
 * @param pCard 
 * @param pNameA 
 * @param pNameB 
 * @param pFollowersA 
 * @param pFollowersB 
 * @param pDeck 
 */
void printData(Card pCard, string pNameA, string pNameB, Deck *pDeck, int pFollowers[]) {
    /* Impresión de información. */
    sleepGame(1);
    cout << " ----- CARD -----\n";
    cout << "Type: " << pCard.getType() << "\nEffect: " << pCard.getEffect() << endl;
    cout << "Cards in Deck: " << pDeck->getCurrentDeckSize() << endl;
    cout << "Cards in Graveyard: " << pDeck->getGraveSize() << endl;
    sleepGame(1);
    cout << " -----PLAYERS IN DRAW-----\n";
    cout << "-> "<< pNameA << ".\n" << "Followers at start: " << pFollowers[1] <<endl;
    cout << "Followers at end: "<< pFollowers[0]<< endl;
    cout << "-> "<< pNameB << ".\n" << "Followers at start: " << pFollowers[3] <<endl;
    cout << "Followers at end: "<< pFollowers[2]<< endl;
    if (pCard.getID() == 4) {
        cout << " ----- NewGod -----\n";
        cout << "Followers: " << pFollowers[0]+pFollowers[2] << endl;
    }
    sleepGame(1);
    cout <<"-----END OF DRAW-----\n";
}

/**
 * @brief He draws a card from the deck and places it in the graveyard.
 * 
 * @param pDeck 
 * @return Card 
 */
Card dropCard(Deck *pDeck) {
    // CARTA
    Card card;
    card = pDeck->getDeck()->top();
    pDeck->minusCurrentDeckSize(1);
    pDeck->plusGraveyard(1);
    pDeck->getGraveyard()->push(card);
    return card;
}

/**
 * @brief Shuffle the deck of cards.
 * 
 * @param pDeck 
 */
void shuffleDeck(stack<Card> *pDeck) {
    vector<Card> *deck = new vector<Card>();
    // Adding to the vector.
    while (!pDeck->empty()) {
        deck->push_back(pDeck->top());
        pDeck->pop();
    }
    // shuffling
    random_shuffle ( deck->begin(), deck->end() );
    // re-inserting deck
    for (vector<Card>::iterator it=deck->begin(); it!=deck->end(); ++it) {
        pDeck->push(*it);
    }
}

/**
 * @brief Apply the card's effect based on its Id.
 * 
 * @param pCard 
 * @param pGodA 
 * @param pGodB 
 * @param pTree 
 * @param pDeck 
 */
void aplicationEffect(Card pCard, int &pGodA, int &pGodB, BiTree &pTree, Deck *pDeck) { 
    switch (pCard.getID()) {
        case 0: //Anarquy
            /* code */
            break;
        case 1: //Betrayal
            betrayalEffect(pGodA, pGodB);
            break;
        case 2: //Death
            deathEffect(pGodA);
            break;
        case 3: //Miracle
            miracleCard(pGodA, pGodB);
            break;
        case 4: //NewGod
            newGodEffect(pTree, pGodA, pGodB);
            break;
        case 5: //Return
            returnEffect(pDeck->getDeck(), pDeck->getGraveyard());
            pDeck->plusCurrentDeckSize(3);
            pDeck->minusGraveyrad(3);
            shuffleDeck(pDeck->getDeck());
            break;
        case 6: //Union
            break;
        default:
            break;
        }
}

/**
 * @brief Gets the first value in the queue by priority and removes it from the queue.
 * 
 * @param pPlayers 
 * @return int 
 */
int asginPlayer(priority_queue<int> *pPlayers) {
    int followers;
    followers = pPlayers->top();
    pPlayers->pop();
    return followers;
}

#endif // _GAME_
