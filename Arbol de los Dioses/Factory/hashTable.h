#if !defined(_HASHTABLE_)
#define _HASHTABLE_

/**
 * @file hashTable.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <string>
#include <queue>
#include "God.h"
using namespace std;

class MapADT {
  private:
    // Atributes.
    const static unsigned int sizeMax = 1000;
    string keys[sizeMax];
    string values[sizeMax];
    God *gods[sizeMax];
  public:
    /**
     * @brief Construct a new MapADT object
     * No parameters.
     */
    MapADT();

    /**
     * @brief Construct a new MapADT object
     * With parameter.
     * @param filename 
     */
    MapADT(string filename);

    // Prototypes of funtions.
    string find(const string& key);  
    void insert(const string& key, const string& value);
    string remove(const string& key);
    unsigned int hash(const string& key);
    int findIndex(const string& key, bool override_duplicated_key);
    void print();
};

/**
 * @brief Construct a new MapADT::MapADT object
 * 
 */
MapADT::MapADT() {
  for (int i = 0; i < sizeMax; i++) {
    keys[i] = string();
    values[i] = string();
    gods[i] = new God();
  }
}

/**
 * @brief Make the hash from the key.
 * 
 * @param pKey 
 * @return unsigned int 
 */
unsigned int MapADT::hash(const string& pKey) {
  unsigned int value = 0 ;
  for (int i = 0; i < pKey.length(); i++)
    value = 37*value + pKey[i];
  return value;
}

/**
 * @brief Show the information in the hash table.
 * 
 */
void MapADT::print() {
  cout << "{\n";
  for (int i = 0; i < sizeMax; i++){
    if (!gods[i]->emptyGod()){
    cout<<gods[i]->getName()<< ":"<<gods[i]->getFollowersQuantity()<<endl;
    }   
  }
  cout << "}\n" << endl;
}

/**
 * @brief Search if the index exists in the hash table.
 * 
 * @param pKey 
 * @param overrideDuplicateKey 
 * @return int 
 */
int MapADT::findIndex(const string& pKey, bool overrideDuplicateKey = true) {     
  unsigned int hashNumber = hash(pKey) % sizeMax, offset = 0, index;
  while (offset < sizeMax) {
    index = (hashNumber + offset) % sizeMax;
    // empty index for new entry with pKey `pKey`
    // or find the index of pKey `pKey` in hash table
    if (keys[index].empty() || (overrideDuplicateKey && keys[index] == pKey)) {
      return index;
    }
    offset++;
  }
  return -1;
}

/**
 * @brief Add a new element to the hash table if the index is founded.
 * 
 * @param pKey 
 * @param pValue 
 */
void MapADT::insert(const string& pKey, const string& pValue) {
  int index = findIndex(pKey);
  if (index == -1) {
    cerr << "Table is full!" << endl;
    return;
  }
  keys[index] = pKey;
  values[index] = pValue;
  gods[index] = new God(pKey, stoi(pValue));
}

/**
 * @brief Search and return the value in the key.
 * 
 * @param pKey 
 * @return string 
 */
string MapADT::find(const string& pKey) {
  int index = findIndex(pKey);
  if (index != -1)
    return values[index];
  return "";
}

/**
 * @brief Search and remove the value according to its key
 * 
 * @param pKey 
 * @return string 
 */
string MapADT::remove(const string& pKey) {
  int index = findIndex(pKey);
  if (index == -1) return "";

  string value = values[index];
  keys[index].clear();
  values[index].clear();

  return value;
}

#endif // _HASHTABLE_
