#if !defined(_CARD_)
#define _CARD_

/**
 * @file Card.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include "God.h"
#include "avlTree.h"

using namespace std;

static int index = 0;
class Card {
    private:
        /* Attributes */
        string typeCard = "";
        string effectString = "";
        int quantity = 0;
        int id = 0;
    public:
        /**
         * @brief Construct a new Card object.
         * With parameters.
         * @param pType 
         * @param pEffect 
         */
        Card(string pType, string pEffect, int id) {
            this->typeCard = pType;
            this->effectString = pEffect;
        }
        /**
         * @brief Construct a new Card object.
         * No parameters.
         */
        Card() {
            this->id = index;
            index++;
        }
        
        // getters & setters.
        string getType() {
            return this->typeCard;
        }
        void setType(string pType) {
            this->typeCard = pType;
        }
        string getEffect() {
            return this->effectString;
        }
        void setEffectString(string pEffect) {
            this->effectString = pEffect;
        }    
        int getID() {
            return this->id;
        }
        void setId(int pId) {
            this->id = pId;
        }
        int getQuantity() {
            return this->quantity;
        }
        void setQuantity(int pQuantity) {
            this->quantity = pQuantity;
        }
};

/**
 * @brief Effect of, miracle.
 * 
 * @param a 
 * @param b 
 */
void miracleCard(int &a, int &b) {
    int auxFollowers = b * 0.2;
    a = a + auxFollowers;
    b = b - auxFollowers;
}

/**
 * @brief Effect of, betrayal.
 * 
 * @param pGodA 
 * @param pGodB 
 */
void betrayalEffect(int &pGodA, int &pGodB) {
    int aFollowers = pGodA;
    int bFollowers = pGodB;
    int auxFollowers = aFollowers * 0.30;

    pGodA = aFollowers - auxFollowers;
    pGodB = bFollowers + auxFollowers;
}

/**
 * @brief Effect of, anarquy.
 * 
 * @param pGodA 
 * @param pGodB 
 */
void anarquiyEffect(int pGodA, int pGodB) {

}

/**
 * @brief Effect of, union.
 * 
 * @param pGodA 
 * @param pGodB 
 */
void unionEfecct(int pGodA, int pGodB) {

}

static int contName = 21; // Represents the name of the new gods.
/**
 * @brief Effect of, new God.
 * 
 * @param pTree 
 * @param pGodA 
 * @param pGodB 
 */
void newGodEffect(BiTree &pTree, int &pGodA, int &pGodB) {  
    int auxFollowers = pGodA + pGodB;
    //cout << "new god: " << contName << "\t followrs:" << auxFollowers << endl;
    deleteAvlNode(pTree, pGodA);
    deleteAvlNode(pTree, pGodB);
    string name = to_string(contName);
    insertAvlTree(pTree, auxFollowers, name);
    contName++;
}

/**
 * @brief Effect of, return.
 * 
 * @param pDeck 
 * @param pGraveyard 
 */
void returnEffect(stack<Card> *pDeck, stack <Card> *pGraveyard) {
    queue<Card> *temp = new queue<Card>();
    int cont = 0;
    while (cont<3) {
        temp->push(pGraveyard->top());
        pGraveyard->pop();
        cont++;
    }
    while (!temp->empty()) {
        pDeck->push(temp->front());
        temp->pop();
    }
}

/**
 * @brief Effect of, death.
 * 
 * @param pGodA 
 */
void deathEffect(int &pGodA) {
    int aFollowers = pGodA;
    int auxFollowers = aFollowers * 0.10;

    pGodA = aFollowers - auxFollowers;
}

#endif // _CARD_