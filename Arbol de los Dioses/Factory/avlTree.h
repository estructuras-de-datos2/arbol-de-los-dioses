#if !defined(_AVLTREE_)
#define _AVLTREE_

/**
 * @file avlTree.h
 * @author Francisco Javier Ovares Rojas.
 * @brief Self balancing avl tree.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "God.h"
#include<iostream>

template<typename T1, typename T2>
constexpr auto max(T1 x, T2 y) { return x>y?x:y; }
 
using namespace std;
 
typedef struct TreeAvl{
    // Atributes
    God *god= new God();
    int data; 
    TreeAvl *leftChild, *rightChild; // Left and right child node.
}BiNode, *BiTree;
 
/**
 * @brief Find out the Height of a node.
 * 
 * @param pNode 
 * @return int 
 */
int binaryTreeHeight(BiTree pNode) {
    if (pNode == NULL) {
        return 0;
    }   
    int left = binaryTreeHeight(pNode->leftChild);
    int right = binaryTreeHeight(pNode->rightChild);
    return max(left, right) + 1;
}
 
/**
 * @brief It goes through the whole tree and turns if there is an imbalance. Single left-left rotation.
 * 
 * @param pNode 
 */
void lLeft(BiTree &pNode) {
    BiTree lch = pNode-> leftChild; // Saves the left child node of the unbalanced node.
    pNode->leftChild = lch->rightChild;
    lch->rightChild = pNode;
    pNode = lch;
}
 
/**
 * @brief Simple Right Right Type Rotation.
 * 
 * @param pNode 
 */
void rRight(BiTree &pNode) {
    BiTree rch = pNode-> rightChild; // Save the right child node of the unbalanced node.
    pNode->rightChild = rch->leftChild;
    rch->leftChild = pNode;
    pNode = rch;
}
 
/**
 * @brief Double rotation of left and right type.
 * 
 * @param pNode 
 */
void lRight(BiTree &pNode) {
    // Make it left-handed first, make it left-left type.
    BiTree lch = pNode->leftChild;
    BiTree lrch = pNode->leftChild->rightChild;
    pNode->leftChild = lrch;
    pNode->leftChild->leftChild = lch;
    pNode-> leftChild-> leftChild-> rightChild = NULL; // Puede haber un error en todo
 
    lLeft(pNode);
}
 
/**
 * @brief Double right and left rotation.
 * 
 * @param pNode 
 */
void rLeft(BiTree &pNode) {
    // First do a right rotation and turn it into a right to right type.
    BiTree rch = pNode->rightChild;
    BiTree rlch = pNode->rightChild->leftChild;
    pNode->rightChild = rlch;
    pNode->rightChild->rightChild = rch;
    pNode->rightChild->rightChild->leftChild = NULL;
 
    rRight(pNode);
}

/**
 * @brief Is in charge of checking the tree and order it according to its height.
 * 
 * @param pNode 
 */
void checkAvlTree(BiTree &pNode) {   
    if (pNode == NULL)
        return;
    if (binaryTreeHeight(pNode->leftChild) - binaryTreeHeight(pNode->rightChild) > 1) {
        if (binaryTreeHeight(pNode->leftChild->leftChild) > binaryTreeHeight(pNode->leftChild->rightChild)) {
            lLeft(pNode);
        } else {
            lRight(pNode);
        }     
    }
    if (binaryTreeHeight(pNode->rightChild) - binaryTreeHeight(pNode->leftChild) > 1) {
        if (binaryTreeHeight(pNode->rightChild->rightChild) > binaryTreeHeight(pNode->rightChild->leftChild)) {
            rRight(pNode);
        }  else {
            rLeft(pNode);
        }    
    }
}
 
/**
 * @brief Assuming no equal data. Insert node. pNode is the root node.
 * 
 * @param pNode 
 * @param pValue 
 * @param pName 
 */
void insertAvlTree (BiTree &pNode, int pValue, string pName) {
    if (pNode == NULL) { // Árbol vacío o nodo hoja
        BiTree newNode = new BiNode;
        newNode->god->setFollowersQuantity(pValue);
        newNode->god->setName(pName);
        newNode->data = pValue;
        newNode->leftChild = NULL;
        newNode->rightChild = NULL;
        pNode = newNode; 
    }
    else {
        if (pValue > pNode->data) {
            insertAvlTree(pNode->rightChild, pValue, pName);
        }  else if ( pValue < pNode->data) {
            insertAvlTree(pNode->leftChild, pValue, pName);
        } else {
            if (pNode->god->getName().compare(pName) == 0) {
                pNode->god->setFollowersQuantity(pValue);
            } else {
                insertAvlTree(pNode->leftChild, pValue, pName);
            }      
        }
    }   
    checkAvlTree(pNode);
}
 
 
/**
 * @brief Remove the node in the avl tree by its value.
 * 
 * @param pNode 
 * @param t 
 * @return BiTree 
 */
BiTree deleteAvlNode(BiTree pNode, int pValue) {
    BiNode *newNode, *temp, *aux, *extra;    
    newNode = pNode;
    temp = NULL;
    while (newNode) {
        if (newNode->data == pValue)
            break;
        temp = newNode;               
        if (pValue > newNode->data)
            newNode = newNode->rightChild;
        else
            newNode = newNode->leftChild;
    }
    if (newNode == NULL)               
        return pNode;
    if (newNode->leftChild == NULL) {
        if (temp == NULL)                  
             pNode = pNode->rightChild;
        else if (temp->leftChild == newNode)
            temp->leftChild = newNode->rightChild;
        else                            
            temp->rightChild = newNode->rightChild;
 
        delete[]newNode;           
    }
    else if (newNode->rightChild == NULL) {
        if (temp == NULL)
            pNode = pNode->leftChild;
        else if (temp->leftChild == newNode)
            temp->leftChild = newNode->leftChild;
        else
            temp->rightChild = newNode->leftChild;
 
        delete[]newNode;
    }
    else {
        extra = newNode;
        aux = newNode->leftChild;
        while (aux->rightChild) {
            extra = aux;              
            aux = aux->rightChild;      
        }
        if (extra == newNode)
            extra->leftChild = aux->leftChild;  
        else    
            extra->rightChild = aux->leftChild;
        newNode->data = aux->data;
        delete[]aux;
    }
    checkAvlTree(pNode);
    return pNode;
}

/**
 * @brief Find the node in the avl tree by its value.
 * 
 * @param pNode 
 * @param t 
 * @return BiTree 
 */
BiTree searchAvlTree(BiTree pNode, int pValue) {
    if (pNode == NULL) {
        return NULL;
    }    
    if (pNode->data == pValue) {
        return pNode;
    } else if (pValue > pNode->data) {
        return searchAvlTree(pNode->rightChild, pValue);
    } else {
        return searchAvlTree(pNode->leftChild, pValue);
    }    
}
 
/**
 * @brief Show AVL tree in postOrder.
 * 
 * @param nodo 
 */
void postOrden(BiTree pNodo){
  	if (pNodo != NULL) {
    	postOrden(pNodo->leftChild);
    	postOrden(pNodo->rightChild);
    	cout<<"\nNIT: "<<pNodo->data<<endl;
		cout<<"Nombre: "<<pNodo->god->getName()<<endl;
		cout<<"Followers: "<<pNodo->god->getFollowersQuantity()<<endl;
  	}
}

#endif // _AVLTREE_
