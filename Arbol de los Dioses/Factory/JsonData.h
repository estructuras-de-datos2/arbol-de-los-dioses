#if !defined(_JASONDATA_)
#define _JASONDATA_

/**
 * @file JsonData.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include "God.h"
#include "hashTable.h"
#include <nlohmann/json.hpp> //Have to install library https://github.com/nlohmann/json#integration

using namespace std;
using json = nlohmann::json;

// Prototypes of functions.
void printJasonData(json pJson);
json updateGodsInformation();
queue<God>* updateGodsStack();

/**
 * @brief Print the Json information.
 * 
 * @param pJson 
 */
void printJasonData(json pJson) {
    // iterate the array
    for (json::iterator it = pJson.begin(); it != pJson.end(); ++it) {
        cout << "Key: " <<it.key() << "\tValue: " << *it << '\n';
    }
}

/**
 * @brief Read the information from the .json file of Gods and load it into a Json file.
 * 
 * @return json 
 */
json updateGodsInformation() {

    std::ifstream jasonF("Files/Gods.json");
    json godsData;
    jasonF >> godsData;
    //printJasonData(godsData);

    return godsData;
}

/**
 * @brief Create a stack of Gods based on the information in the Json.
 * 
 * @return queue<God>* 
 */
queue<God>* updateGodsStack() {
    json godsInfo = updateGodsInformation();
    queue<God> *gods = new queue<God>();
    int cont = 0;
    for (json::iterator it = godsInfo.begin(); it != godsInfo.end(); ++it) {
        God *newGod = new God();
        newGod->setFollowersQuantity(it.value());
        newGod->setName(it.key());
        newGod->setNumberOfGod(++cont);
        gods->push(*newGod);
        //cout << it.key() <<"  " << *it <<endl;
        //cout << newGod->getName() <<" " << newGod->getFollowersQuantity()<<endl;
    }

    return gods;
}

/**
 * @brief Read the information from the .json file of Cards and load it into a Json file.
 * 
 * @return json 
 */
json updateCardsInformation() {
    std::ifstream jasonFile("Files/Cards.json");
    json cardsDataJson;
    jasonFile >> cardsDataJson;

    return cardsDataJson;
}

/**
 * @brief Create a stack of Cards based on the information in the Json.
 * 
 * @param pStack 
 */
void updatePrimalCards(stack<Card> *&pStack) {
    int quantity[] = {7, 4, 10, 5, 4, 6, 4};
    int index=0;
    json cardsInfo = updateCardsInformation();
    for (json::iterator it = cardsInfo.begin(); it != cardsInfo.end(); ++it) {
        Card *newCard = new Card();
        newCard->setType(it.key());
        newCard->setEffectString(*it);
        newCard->setQuantity(quantity[index]);
        pStack->push(*newCard);
        //cout << it.key() <<"  " << *it << "  "<< quantity[index] << "id:" << newCard->getID()<<endl;
        index++;
    }
}

#endif // _JASONDATA_
