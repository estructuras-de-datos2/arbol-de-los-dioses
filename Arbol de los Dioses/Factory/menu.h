#if !defined(_MENU_)
#define _MENU_

/**
 * @file menu.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include "sleep.h"

using namespace std;

/**
 * @brief Title text string.
 * 
 * @return string 
 */
string title() {
    string title = "\n¡WAR OF GODS!\n";
    return title;
}

/**
 * @brief Options of the menu text string.
 * 
 * @return string 
 */
string options() {
    string options = "---------------------------\n";
    options += "1. Play a Card.\n";
    options += "2. Show Hash Table.\n" ;
    options += "3. Stop the Game..\n";
    options += "---------------------------\n";
    return options;
}

/**
 * @brief Choice text string.
 * 
 * @return string 
 */
string choice() {
    string choice = "Enter your choice: ";
    return choice;
}

/**
 * @brief Leave text string.
 * 
 * @return string 
 */
string farewell() {
    string godbye = "\n------------------\n";
    godbye += "¡GOOD BYE TRAVELER!\n";
    godbye += "------------------\n";
    return godbye;
}

/**
 * @brief prints the text strings.
 * 
 */
void menu() {
    cout << title();
    //sleepGame(1);
    cout << options();
    //sleepGame(1);
    cout << choice();
}

#endif // _MENU_
