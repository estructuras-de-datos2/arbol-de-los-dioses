#if !defined(_SLEPP_)
#define _SLEPP_

/**
 * @file sleep.h
 * @author Francisco Javier Ovares Rojas.
 * @version 0.1
 * @date 2021-11-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

using namespace std;

/**
 * @brief Pause the system for the requested number of seconds.
 * 
 * @param pTime 
 */
void sleepGame(int pTime) {
    for (int i=1; i>0; --i) {
        this_thread::sleep_for (std::chrono::seconds(pTime));
    }
}

#endif // _SLEPP_
